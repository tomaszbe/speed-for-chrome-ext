const lincor = require('lincor')
const { Car } = lincor
const analyse = require('./src/trackAnalyser')
const moment = require('moment')
const express = require('express')
const fs = require('fs')

lincor.init(require('./credentials.json'))

const objects = require('./allObjects.json')
Car.setAvailableObjects(objects)

function spansOverlap(aStart, aEnd, bStart, bEnd) {
  return aStart.isBefore(bEnd) && aEnd.isAfter(bStart)
}

async function analysePlateAtDate(plate, start, end) {
  const car = Car.findByPlateNumber(plate)

  if (car === undefined) {
    return {
      text: 'Nie znaleziono samochodu w systemie Lincor.'
    }
  }

  let tracks = await car.getTracks(start, end)
  tracks = tracks.filter(track =>
    spansOverlap(start, end, track.getStartDate(), track.getStopDate())
  )

  const resultPromises = tracks.map(async track => {
    const filePath = `./cache/${plate}/${track.getId()}.json`
    if (fs.existsSync(filePath)) {
      console.log('Retrieving from cache :)')
      return JSON.parse(fs.readFileSync(filePath).toString())
    }

    const result = {
      track: track.trackInfo,
      result: await analyse(track)
    }

    if (!fs.existsSync(`./cache/${plate}`)) {
      fs.mkdirSync(`./cache/${plate}`)
    }

    fs.writeFileSync(filePath, JSON.stringify(result))

    return result
  })

  const results = await Promise.all(resultPromises)

  return results
}

const app = express()
app.use(require('cors')())

app.get('/', async (req, res) => {
  try {
    const plate = req.query.plate
    const start = moment(req.query.start)
    const end = req.query.end ? moment(req.query.end) : moment()

    const result = await analysePlateAtDate(plate, start, end)
    res.send(result)
  } catch (error) {
    console.error(error)
    res.status(400).send({
      message: error.message
    })
  }
})

app.listen(80, () => console.log('Listening...'))
