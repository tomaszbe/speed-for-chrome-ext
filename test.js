const { getWaysAround } = require('./src/overpass')
const {
  waysWithinRadius,
  wayDistanceSq,
  wayWithHighestMaxspeed
} = require('./src/mapMatcher')

// 52.03369249347778 21.102198997381848 82 42904375 30

const point = {
  lat: 52.9497643,
  lon: 14.9563845,
  speed: 167
}

const track = [point]

;(async () => {
  const ways = await getWaysAround(track, 20)
  const withinRadius = waysWithinRadius(ways, point, 20)

  withinRadius.forEach(way => {
    console.log(
      way.id,
      way.tags.maxspeed,
      Math.sqrt(wayDistanceSq(point, way)) * 111000
    )
  })
})()
