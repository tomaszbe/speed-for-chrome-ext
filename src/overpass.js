const axios = require('axios')
const http = axios.create({
  baseURL: 'https://overpass.kumi.systems/api/interpreter'
})

async function getWaysAround(track, radius = 20) {
  const latLons = track.map(point => point.lat + ',' + point.lon).join(',')
  const data = `[out:json];
    (
      way(around:${radius},${latLons})[highway][maxspeed];
    );
    out tags qt geom;`

  const response = await http.get('', { data })
  return response.data.elements
}

exports.getWaysAround = getWaysAround
