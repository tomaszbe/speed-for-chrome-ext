const distance = require('./distanceSquared')

function dot(u, v) {
  return u[0] * v[0] + u[1] * v[1]
}

const pointToSegmentDistanceSquared = (p, a, b) => {
  const v = [b[0] - a[0], b[1] - a[1]]
  const w = [p[0] - a[0], p[1] - a[1]]

  const c1 = dot(w, v)
  if (c1 <= 0) {
    return distance(p, a)
  }
  const c2 = dot(v, v)
  if (c2 <= c1) {
    return distance(p, b)
  }
  const b2 = c1 / c2
  const Pb = [a[0] + b2 * v[0], a[1] + b2 * v[1]]
  return distance(p, Pb)
}

module.exports = pointToSegmentDistanceSquared
