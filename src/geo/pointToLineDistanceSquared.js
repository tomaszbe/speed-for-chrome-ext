const pointToSegmentDistance = require('./pointToSegmentDistanceSquared')

const pointToLineDistance = (p, lineString) => {
  let distance = Infinity

  for (let i = 0; i < lineString.length - 1; ++i) {
    let a = lineString[i]
    let b = lineString[i + 1]
    let d = pointToSegmentDistance(p, a, b)
    if (d < distance) {
      distance = d
    }
  }

  return distance
}

module.exports = pointToLineDistance