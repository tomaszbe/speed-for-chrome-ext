const PI_180 = Math.PI / 180

const degreesToRadians = degrees => degrees * PI_180

const distanceSquared = (p1, p2, cosLat) => {
  const dLat = p2[0] - p1[0]
  const dLon =
    (p2[1] - p1[1]) *
    (cosLat !== undefined ? cosLat : Math.cos(degreesToRadians(p1[0])))

  return dLat * dLat + dLon * dLon
}

module.exports = distanceSquared
