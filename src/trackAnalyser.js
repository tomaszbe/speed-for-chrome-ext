const { chunkify, dechunkify } = require('./utils')
const { mapPointsToWays } = require('./mapMatcher')

async function mapMatch(points) {
  if (points.length === 0) {
    return []
  }
  const chunks = chunkify(points, 50)
  const mapMatchPromises = chunks.map(mapPointsToWays)
  const mapMatchResults = await Promise.all(mapMatchPromises)
  const mappedPoints = dechunkify(mapMatchResults)
  return mappedPoints
}

function isAggresive(point) {
  if (!point.accelerometers) {
    return false
  }
  return Object.values(point.accelerometers).some(
    accel => Math.abs(accel) > 2
  )
}

function calculateScore(mappedPoints) {
  let status = 'OK'
  let data = {
    doubles: [],
    over30s: [],
    aggresive: []
  }

  mappedPoints.forEach(({ point, way }) => {
    if (way === undefined) {
      return
    }
    const speed = point.speed
    const speedLimit = Number(way.tags.maxspeed)

    if (speed >= speedLimit * 2) {
      data.doubles.push({ point, way })
    }

    if (speed > speedLimit + 30) {
      data.over30s.push({ point, way })
    }

    if (isAggresive(point)) {
      data.aggresive.push({ point, way })
    }
  })

  if (data.doubles.length || data.over30s.length || data.aggresive.length) {
    status = 'NOT OK'
  }

  return {
    status,
    data
  }
}

async function analyse(track) {
  console.log('Waiting for points...')
  let points = await track.getPoints()
  console.log('Got', points.length, 'points.')

  points = points.filter(point => point.speed > 60)
  console.log('Filtering...', points.length, 'remaining.')

  const mappedPoints = await mapMatch(points)

  return calculateScore(mappedPoints)
}

module.exports = analyse
