function chunkify(array, chunkSize) {
  const chunks = []
  const numberOfChunks = Math.ceil(array.length / chunkSize)
  for (let i = 0; i < numberOfChunks; ++i) {
    const chunk = array.slice(i * chunkSize, (i + 1) * chunkSize)
    chunks.push(chunk)
  }
  return chunks
}

function dechunkify(chunks) {
  return [].concat.apply([], chunks)
}

exports.chunkify = chunkify
exports.dechunkify = dechunkify