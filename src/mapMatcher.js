const lineDistanceSq = require('./geo/pointToLineDistanceSquared')

const PI_180 = Math.PI / 180

const degreesToRadians = degrees => degrees * PI_180

function wayToLineString(way) {
  return way.geometry.map(point => [point.lat, point.lon])
}

function wayDistanceSq(point, way) {
  return lineDistanceSq([point.lat, point.lon], wayToLineString(way))
}

function isPointWithinBounds(point, bounds, tolerance) {
  const deltaLat = tolerance / 111000
  const deltaLon = tolerance / (111000 * Math.cos(degreesToRadians(point.lat)))

  if (
    point.lat > bounds.maxlat + deltaLat ||
    point.lat < bounds.minlat - deltaLat ||
    point.lon > bounds.maxlon + deltaLon ||
    point.lon < bounds.minlon - deltaLon
  ) {
    return false
  }
  return true
}

function isWayWithinRadius(point, way, radius = 20) {
  if (!isPointWithinBounds(point, way.bounds, radius)) {
    return false
  }

  const radiusInDegrees = radius / 111000
  const radiusInDegreesSq = radiusInDegrees * radiusInDegrees
  return wayDistanceSq(point, way) < radiusInDegreesSq
}

function waysWithinRadius(ways, point, radius = 20) {
  return ways.filter(way => isWayWithinRadius(point, way, radius))
}

function wayWithHighestMaxspeed(ways) {
  let highestMaxSpeed = 0
  let result = undefined

  for (way of ways) {
    const wayMaxSpeed = Number(way.tags.maxspeed)
    if (wayMaxSpeed > highestMaxSpeed) {
      highestMaxSpeed = wayMaxSpeed
      result = way
    }
  }

  return result
}

function snapPointToWay(point, ways, radius = 20) {
  const wayCandidates = waysWithinRadius(ways, point, radius)
  return wayWithHighestMaxspeed(wayCandidates)
}

const { getWaysAround } = require('./overpass')

async function mapPointsToWays(points) {
  const ways = await getWaysAround(points)

  const mapped = points.map(point => {
    const way = snapPointToWay(point, ways, 20)
    return {
      point,
      way
    }
  })

  const filtered = mapped.filter(
    ({ point, way }) => way && point.speed - way.tags.maxspeed > 0
  )

  return filtered
}

exports.wayDistanceSq = wayDistanceSq
exports.mapPointsToWays = mapPointsToWays
exports.waysWithinRadius = waysWithinRadius
exports.wayWithHighestMaxspeed = wayWithHighestMaxspeed
